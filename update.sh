#!/bin/bash

php artisan down

git fetch --all
git pull origin master
git checkout $i

composer install --no-dev --no-interaction
php artisan migrate
php artisan db:seed --class=UpdateSeeder

php artisan up
