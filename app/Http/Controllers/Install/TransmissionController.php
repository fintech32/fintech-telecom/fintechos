<?php

namespace App\Http\Controllers\Install;

use App\Http\Controllers\Controller;
use App\Models\Config;
use App\Service\ShellCommand;
use Transmission\Client;


class TransmissionController extends Controller
{
    //Installation du service transmission par docker
    public function install()
    {
        try {
            ShellCommand::execute("docker pull lscr.io/linuxserver/transmission:latest");
            return response()->json();
        } catch (\Exception $exception) {
            return response()->json($exception, 500);
        }
    }

    public function start_t()
    {
        try {
            $install = ShellCommand::execute("docker run -d \
  --name=transmission \
  -e PUID=1000 \
  -e PGID=1000 \
  -e TZ=Europe/London \
  -e TRANSMISSION_WEB_HOME=/combustion-release/ `#optional` \
  -p 9091:9091 \
  -p 51413:51413 \
  -p 51413:51413/udp \
  -v " . public_path('/storage/download') . "/data:/config \
  -v " . public_path('/storage/download') . "/downloads:/downloads \
  -v " . public_path('/storage/download') . "/watch:/watch \
  --restart unless-stopped \
  lscr.io/linuxserver/transmission:latest");
            return response()->json();
        } catch (\Exception $exception) {
            return response()->json($exception, 500);
        }
    }

    public function install_php()
    {
        try {
            $cmd = ShellCommand::execute("cd ../ && composer require mmockelyn/php-transmission --no-interaction --ignore-platform-reqs");
            return response()->json($cmd);
        }catch (\Exception $exception) {
            return response()->json($exception, 500);
        }
    }

    public function test_transmission()
    {
        try {
            fsockopen(config('app.app_ip').':9091');
            try {
                $transmission = new Client(config('app.app_ip'), 9091);
                $transmission->get();
                Config::query()->first()->update(["transmission_install" => true]);
                return response()->json();
            }catch (\Exception $exception) {
                return response()->json($exception->getMessage(), 500);
            }
        }catch (\Exception $exception) {
            return response()->json($exception->getMessage(), 500);
        }
    }

}
