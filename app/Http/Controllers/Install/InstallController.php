<?php

declare(strict_types=1);

namespace App\Http\Controllers\Install;

use App\Http\Controllers\Controller;
use App\Models\Config;
use App\Models\User;
use App\Models\Wifi;
use Illuminate\Http\Request;
use Hash;

final class InstallController extends Controller
{
    public function verifyConnectivity()
    {
        return view('install.verif_connectivity');
    }

    public function administrateur()
    {
        return view('install.administrateur');
    }

    public function wifi()
    {
        return view('install.wifi');
    }

    public function store(Request $request)
    {
        return match ($request->get('action')) {
            "paramAdmin" => $this->paramAdmin($request),
            "paramWifi" => $this->paramWifi($request)
        };
    }

    private function paramAdmin(Request $request)
    {
        User::create([
            "name" => $request->get('name'),
            "password" => Hash::make($request->get('password'))
        ]);

        return redirect()->route('install.wifi');
    }

    private function paramWifi(Request $request)
    {
        $wifis = Wifi::all();

        foreach ($wifis as $wifi) {
            $wifi->update([
                "ssid" => $request->get('ssid'),
                "key" => encrypt($request->get('key'))
            ]);
        }

        Config::query()->first()->update(["installer" => true]);

        return redirect()->to('/');
    }
}
