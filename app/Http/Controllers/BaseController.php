<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Models\Config;
use App\Models\Statement;
use Transmission\Client;

class BaseController extends Controller
{
    public function __construct()
    {
        $this->middleware(["auth"]);
    }

    public function index()
    {
        return view('dashboard');
    }

    public function box_status()
    {
        $config = Config::query()->first();
        $statement = Statement::query()->first();

        return view('box_status', compact('config', 'statement'));
    }

    public function box_download()
    {
        if(Config::query()->first()->transmission_install) {
            $transmission = new Client(config('app.app_ip'), 9091);
            $transmissions = $transmission->get();
        } else {
            $transmissions = null;
        }
        //dd($transmissions);
        return view('box_download', compact('transmissions'));
    }
}
