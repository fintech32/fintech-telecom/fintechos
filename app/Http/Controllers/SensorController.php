<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use Faker\Factory;
use Illuminate\Http\Request;

final class SensorController extends Controller
{
    public $faker;

    public function __construct()
    {
        $this->faker = Factory::create('fr_FR');
    }

    public function sensor(Request $request)
    {
        return match ($request->get('sensor')) {
            "tempHdd" => $this->temperature_hdd(),
            "tempSwitch" => $this->temperature_switch(),
            "tempCpua" => $this->temperature_cpua(),
            "tempCpub" => $this->temperature_cpub(),
            "sensorVone" => $this->sensorVone(),
        };
    }

    private function temperature_hdd()
    {
        return rand(30, 75)."°C";
    }

    private function temperature_switch()
    {
        return rand(30, 50)."°C";
    }

    private function temperature_cpua()
    {
        return rand(50, 75)."°C";
    }

    private function temperature_cpub()
    {
        return rand(50, 75)."°C";
    }

    private function sensorVone()
    {
        return rand(2070, 3120)." Tours/min";
    }
}
