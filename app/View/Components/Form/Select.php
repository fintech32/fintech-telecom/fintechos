<?php

declare(strict_types=1);

namespace App\View\Components\Form;

use Illuminate\View\Component;
use Closure;

final class Select extends Component
{
    public $name;

    public $datas;

    public $label;

    /**
     * @var null
     */
    public $placeholder;

    /**
     * @var bool
     */
    public $required;
    public ?array $value;

    /**
     * Create a new component instance.
     *
     * @param $name
     * @param $datas
     * @param $label
     * @param null $placeholder
     * @param bool $required
     * @param string|int|null $value
     */
    public function __construct($name, $datas, $label, $placeholder = null, bool $required = false, string|int|null $value = null)
    {

        $this->name = $name;
        $this->datas = $datas;
        $this->label = $label;
        $this->placeholder = $placeholder;
        $this->required = $required;
        $this->value = $value;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|Closure|string
     */
    public function render()
    {
        return view('components.form.select');
    }
}
