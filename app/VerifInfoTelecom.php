<?php

namespace App;

use App\Models\Config;
use App\Models\LogConnect;
use App\Models\Statement;
use App\Service\Telecom;
use App\Test\TestingSystem;
use Faker\Factory;

class VerifInfoTelecom
{
    public static function GetInfoBox()
    {
        if(config('app.env') == 'local') {
            TestingSystem::testing_get_info_box();
        } else {
            $telecom = new Telecom();
            $faker = Factory::create('fr_FR');

            $xdsl = $telecom->authentification(Config::query()->first()->customer_code)->data->xdsl;
            LogConnect::query()->create([
                "type" => "connexion",
                "lien" => $xdsl->type_access,
                "status" => "connecter"
            ]);
            if ($xdsl->status == 'active') {
                Config::query()->first()->update([
                    "mac_address" => $xdsl->modems[0]->mac_address,
                    "serial_number" => $xdsl->modems[0]->serial_number,
                    "type_connect" => $xdsl->type_access
                ]);

                Statement::query()->first()->update([
                    "stat_internet" => "ok",
                    "stat_auth" => "ok",
                    "stat_telephone" => $xdsl->line->status == 'ok' ? 'ok' : 'echec',
                    "etat_connexion_internet" => self::pingInternet() ? 'connecter' : 'deconnecter',
                    "etat_connexion_ftth" => $xdsl->type_access == 'ftth' ? (self::pingInternet() ? 'connecter' : 'deconnecter') : null,
                    "etat_connexion_adsl" => $xdsl->type_access == 'adsl' ? (self::pingInternet() ? 'connecter' : 'deconnecter') : null,
                    "type_connexion" => $xdsl->type_access,
                    "ipv4" => $xdsl->ips->ip_address,
                    "ipv6" => $xdsl->ipv6enable ? $faker->ipv6 : null,
                    "recu" => 0,
                    "emis" => 0,
                    "debitDown" => $xdsl->type_access == 'adsl' ? rand(10000, 35000) : rand(390000, 780000),
                    "debitUp" => $xdsl->type_access == 'adsl' ? rand(10000, 35000) : rand(390000, 780000),
                    "debitDownMax" => $xdsl->type_access == 'adsl' ? 800000 : 10000000,
                    "debitUpMax" => $xdsl->type_access == 'adsl' ? 45000 : 800000,
                    "module_connect" => $xdsl->type_access == 'ftth',
                    "start_adsl_synchro" => $xdsl->type_access == "adsl" ? now() : null
                ]);
                LogConnect::query()->create([
                    "type" => "lien",
                    "lien" => $xdsl->type_access,
                    "status" => "connecter"
                ]);
            } else {
                Statement::query()->first()->update([
                    "stat_internet" => "echec",
                    "stat_auth" => "echec",
                    "stat_telephone" => 'echec',
                    "etat_connexion_internet" => 'deconnecter',
                    "etat_connexion_ftth" => 'deconnecter',
                    "etat_connexion_adsl" => 'deconnecter',
                    "type_connexion" => $xdsl->type_access,
                    "ipv4" => $xdsl->ips->ip_address,
                    "ipv6" => $xdsl->ipv6enable ? $faker->ipv6 : null,
                    "recu" => 0,
                    "emis" => 0,
                    "debitDown" => 0,
                    "debitUp" => 0,
                    "debitDownMax" => 0,
                    "debitUpMax" => 0,
                    "module_connect" => false,
                ]);
                LogConnect::query()->create([
                    "type" => "lien",
                    "lien" => $xdsl->type_access,
                    "status" => "deconnecter"
                ]);
            }
        }
    }

    public static function updateSystem()
    {
        $telecom = new Telecom();
        $xdsl = $telecom->authentification(Config::query()->first()->customer_code)->data->xdsl;
        $config = Config::query()->first();

        if($config->version_micrologiciel < $xdsl->modems[0]->firmwareVersion) {
            $process = \Symfony\Component\Process\Process::fromShellCommandline('/bin/bash ./update.sh');
            if(!$process->getExitCode()) {
                $config->update(["version_micrologiciel" => $xdsl->modems[0]->firmwareVersion]);
                return response()->json();
            } else {
                return response()->json(null, 500);
            }
        }
    }

    public static function pingInternet()
    {
        $fp = @fsockopen("www.google.com", 80, $errno, $errstr, 30);
        if (!$fp) {
            return false;
        } else {
            return true;
        }
    }

    public static function convertByte($val, $type_val, $type_wanted)
    {
        $tab_val = array("o", "ko", "Mo", "Go", "To", "Po", "Eo");
        if (!(in_array($type_val, $tab_val) && in_array($type_wanted, $tab_val)))
            return 0;
        $tab = array_flip($tab_val);
        $diff = $tab[$type_val] - $tab[$type_wanted];
        if ($diff > 0)
            return (number_format($val * pow(1024, $diff), 2, '.', ' '));
        if ($diff < 0)
            return (number_format($val / pow(1024, -$diff), 2, '.', ' '));
        return (number_format($val, 2, '.', ' '));
    }
}
