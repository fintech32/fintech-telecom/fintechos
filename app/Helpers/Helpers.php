<?php

namespace App\Helpers;

class Helpers
{
    public static function getStatusTransmission($status)
    {
        return match ($status) {
            0 => "En Pause",
            1 => null,
            2 => null,
            3 => null,
            4 => "Téléchargement en cours"
        };
    }
}
