<?php

declare(strict_types=1);

namespace App\Service;

use Symfony\Component\Process\Process;
use Exception;
use Log;

final class ShellCommand
{
    public static function execute($cmd): string
    {
        $process = Process::fromShellCommandline($cmd);
        $output = '';

        $capture = function ($type, $line) use (&$output): void {
            $output .= $line;
        };

        $process->setTimeout(null)
            ->run($capture);

        if ($process->getExitCode()) {
            $exeption = new Exception($cmd." - ".$output, 500);
            report($exeption);
            Log::critical($exeption->getMessage());
            return $exeption->getMessage();
        } else {
            return $output;
        }

    }
}
