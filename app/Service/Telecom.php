<?php

declare(strict_types=1);

namespace App\Service;

use Http;

final class Telecom
{
    private string $endpoint;

    public function __construct()
    {
        $this->endpoint = config('app.telecom_endpoint').'/api';
    }

    public function connexion_serveur($customer_code_id)
    {
        return Http::withoutVerifying()->post($this->endpoint.'/ping', ["customer_code_id" => $customer_code_id])->status();
    }

    public function authentification($customer_code_id)
    {
        return Http::withoutVerifying()->post($this->endpoint.'/box/auth', ["customer_code_id" => $customer_code_id])->object();
    }
}
