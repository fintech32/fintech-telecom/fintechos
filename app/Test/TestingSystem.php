<?php

namespace App\Test;

use App\Models\Config;
use App\Models\LogConnect;
use App\Models\Statement;
use App\VerifInfoTelecom;
use Faker\Factory;

class TestingSystem
{
    public static function testing_get_info_box()
    {
        $faker = Factory::create('fr_FR');
        $type_access = 'adsl';
        $xdsl_status = $faker->boolean;
        $line_status = $faker->boolean;

        if($xdsl_status) {
            Config::query()->first()->update([
                "mac_address" => $faker->macAddress,
                "serial_number" => $faker->randomNumber(3).'-'.$faker->randomNumber(7).'-'.$faker->randomNumber(5),
                "type_connect" => $type_access
            ]);

            Statement::query()->first()->update([
                "stat_internet" => "ok",
                "stat_auth" => "ok",
                "stat_telephone" => $line_status == 'ok' ? 'ok' : 'echec',
                "etat_connexion_internet" => VerifInfoTelecom::pingInternet() ? 'connecter' : 'deconnecter',
                "etat_connexion_ftth" => $type_access == 'ftth' ? (VerifInfoTelecom::pingInternet() ? 'connecter' : 'deconnecter') : null,
                "etat_connexion_adsl" => $type_access == 'adsl' ? (VerifInfoTelecom::pingInternet() ? 'connecter' : 'deconnecter') : null,
                "type_connexion" => $type_access,
                "ipv4" => $faker->ipv4,
                "ipv6" => $faker->boolean ? $faker->ipv6 : null,
                "recu" => 0,
                "emis" => 0,
                "debitDown" => $type_access == 'adsl' ? rand(10000, 35000) : rand(390000, 780000),
                "debitUp" => $type_access == 'adsl' ? rand(10000, 35000) : rand(390000, 780000),
                "debitDownMax" => $type_access == 'adsl' ? 800000 : 10000000,
                "debitUpMax" => $type_access == 'adsl' ? 45000 : 800000,
                "module_connect" => $type_access == 'ftth',
                "start_adsl_synchro" => $type_access == "adsl" ? now() : null
            ]);

            LogConnect::query()->create([
                "type" => "lien",
                "lien" => $type_access,
                "status" => "connecter"
            ]);
        } else {
            Statement::query()->first()->update([
                "stat_internet" => "echec",
                "stat_auth" => "echec",
                "stat_telephone" => 'echec',
                "etat_connexion_internet" => 'deconnecter',
                "etat_connexion_ftth" => 'deconnecter',
                "etat_connexion_adsl" => 'deconnecter',
                "type_connexion" => $type_access,
                "ipv4" => $faker->ipv4,
                "ipv6" => $faker->boolean ? $faker->ipv6 : null,
                "recu" => 0,
                "emis" => 0,
                "debitDown" => 0,
                "debitUp" => 0,
                "debitDownMax" => 0,
                "debitUpMax" => 0,
                "module_connect" => false,
            ]);
            LogConnect::query()->create([
                "type" => "lien",
                "lien" => $type_access,
                "status" => "deconnecter"
            ]);
        }
    }
}
