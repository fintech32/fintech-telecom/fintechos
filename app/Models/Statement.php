<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Statement
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon $start_at
 * @property string $stat_internet
 * @property string $stat_auth
 * @property string $stat_telephone
 * @property string $etat_connexion_internet
 * @property string $etat_connexion_ftth
 * @property string $etat_connexion_adsl
 * @property string $type_connexion
 * @property string $ipv4
 * @property string $ipv6
 * @property float $recu
 * @property float $emis
 * @property float $debitDown
 * @property float $debitUp
 * @property float $debitDownMax
 * @property float $debitUpMax
 * @property int $module_connect
 * @method static \Illuminate\Database\Eloquent\Builder|Statement newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Statement newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Statement query()
 * @method static \Illuminate\Database\Eloquent\Builder|Statement whereDebitDown($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Statement whereDebitDownMax($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Statement whereDebitUp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Statement whereDebitUpMax($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Statement whereEmis($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Statement whereEtatConnexionAdsl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Statement whereEtatConnexionFtth($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Statement whereEtatConnexionInternet($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Statement whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Statement whereIpv4($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Statement whereIpv6($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Statement whereModuleConnect($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Statement whereRecu($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Statement whereStartAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Statement whereStatAuth($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Statement whereStatInternet($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Statement whereStatTelephone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Statement whereTypeConnexion($value)
 * @property-read mixed $stat_auth_format
 * @property-read mixed $stat_internet_format
 * @property-read mixed $etat_connexion_adsl_format
 * @property-read mixed $etat_connexion_ftth_format
 * @property-read mixed $etat_connexion_internet_format
 * @property-read mixed $stat_telephone_format
 * @property \Illuminate\Support\Carbon|null $start_adsl_synchro
 * @property-read mixed $module_connect_format
 * @method static \Illuminate\Database\Eloquent\Builder|Statement whereStartAdslSynchro($value)
 * @mixin \Eloquent
 */
final class Statement extends Model
{
    protected $guarded = [];
    public $timestamps = false;
    protected $dates = ["start_at", "start_adsl_synchro"];
    protected $appends = [
        "stat_internet_format",
        "stat_auth_format",
        "stat_telephone_format",
        "etat_connexion_internet_format",
        "etat_connexion_ftth_format",
        "etat_connexion_adsl_format",
        "module_connect_format",
    ];

    public function getStatInternetFormatAttribute()
    {
        if ('ok' === $this->stat_internet) {
            return "<span class='text-success'>OK</span>";
        } else {
            return "<span class='text-danger'>Echec</span>";
        }
    }

    public function getStatAuthFormatAttribute()
    {
        if ('ok' === $this->stat_auth) {
            return "<span class='text-success'>OK</span>";
        } else {
            return "<span class='text-danger'>Echec</span>";
        }
    }

    public function getStatTelephoneFormatAttribute()
    {
        if ('ok' === $this->stat_telephone) {
            return "<span class='text-success'>OK</span>";
        } else {
            return "<span class='text-danger'>Echec</span>";
        }
    }

    public function getEtatConnexionInternetFormatAttribute()
    {
        if ('connecter' === $this->etat_connexion_internet) {
            return "<span class='text-success'><i class='fa fa-check-circle text-success'></i> Connecté</span>";
        } else {
            return "<span class='text-danger'><i class='fa fa-xmark-circle text-danger'></i> Déconnecté</span>";
        }
    }

    public function getEtatConnexionFtthFormatAttribute()
    {
        if ('connecter' === $this->etat_connexion_ftth) {
            return "<span class='text-success'><i class='fa fa-check-circle text-success'></i> Connecté</span>";
        } else {
            return "<span class='text-danger'><i class='fa fa-xmark-circle text-danger'></i> Déconnecté</span>";
        }
    }

    public function getEtatConnexionAdslFormatAttribute()
    {
        if ('connecter' === $this->etat_connexion_adsl) {
            return "<span class='text-success'><i class='fa fa-check-circle text-success'></i> Connecté</span>";
        } else {
            return "<span class='text-danger'><i class='fa fa-xmark-circle text-danger'></i> Déconnecté</span>";
        }
    }

    public function getModuleConnectFormatAttribute()
    {
        if ($this->module_connect) {
            return "<span class='text-success'><i class='fa fa-check-circle text-success'></i> Connecté</span>";
        } else {
            return "<span class='text-danger'><i class='fa fa-xmark-circle text-danger'></i> Déconnecté</span>";
        }
    }
}
