<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Config
 *
 * @property int $id
 * @property int $installer
 * @property string $version_micrologiciel
 * @property string|null $mac_address
 * @property string|null $serial_number
 * @property string $type_connect
 * @property string|null $customer_code
 * @method static \Illuminate\Database\Eloquent\Builder|Config newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Config newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Config query()
 * @method static \Illuminate\Database\Eloquent\Builder|Config whereCustomerCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Config whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Config whereInstaller($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Config whereMacAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Config whereSerialNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Config whereTypeConnect($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Config whereVersionMicrologiciel($value)
 * @property int $transmission_install
 * @method static \Illuminate\Database\Eloquent\Builder|Config whereTransmissionInstall($value)
 * @mixin \Eloquent
 */
final class Config extends Model
{
    protected $guarded = [];
    public $timestamps = false;
}
