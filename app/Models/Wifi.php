<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Wifi
 *
 * @property int $id
 * @property string $name_connection
 * @property string $bande
 * @property string $canal_primary
 * @property int $active
 * @property string $ssid
 * @property string $type_protection
 * @property string $key
 * @property int $wps
 * @method static \Illuminate\Database\Eloquent\Builder|Wifi newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Wifi newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Wifi query()
 * @method static \Illuminate\Database\Eloquent\Builder|Wifi whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Wifi whereBande($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Wifi whereCanalPrimary($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Wifi whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Wifi whereKey($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Wifi whereNameConnection($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Wifi whereSsid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Wifi whereTypeProtection($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Wifi whereWps($value)
 * @mixin \Eloquent
 */
final class Wifi extends Model
{
    protected $guarded = [];
    public $timestamps = false;
}
