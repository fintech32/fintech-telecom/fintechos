<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\LogConnect
 *
 * @property int $id
 * @property string $type
 * @property string $lien
 * @property string $status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|LogConnect newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|LogConnect newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|LogConnect query()
 * @method static \Illuminate\Database\Eloquent\Builder|LogConnect whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LogConnect whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LogConnect whereLien($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LogConnect whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LogConnect whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LogConnect whereUpdatedAt($value)
 * @mixin \Eloquent
 */
final class LogConnect extends Model
{
    protected $guarded = [];
}
