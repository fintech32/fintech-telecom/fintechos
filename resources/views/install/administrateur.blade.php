@extends("install.layout")

@section("content")
    <!--begin::Wrapper-->
    <div class="card card-flush w-lg-650px py-5">
        <div class="card-body py-15 py-lg-10">
            <!--begin::Logo-->
            <div class="mb-14">
                <a href="/install" class="">
                    <img alt="Logo" src="/storage/logo_carre.png" class="h-100px" />
                </a>
            </div>
            <!--end::Logo-->
            <!--begin::Title-->
            <h1 class="fw-bolder text-gray-900 mb-5">Configuration 1/2</h1>
            <!--end::Title-->
            <!--begin::Message-->
            <div class="fs-6 fw-semibold text-gray-500 mb-10 mt-3">
                Administrateur
            </div>

            <form action="{{ route('install.store') }}" class="my-5" method="POST">
                @csrf
                <input type="hidden" name="action" value="paramAdmin">
                <x-form.input
                    name="name"
                    label="Pseudo"
                    value="Administrateur"
                    required="true" />

                <x-form.input
                    type="password"
                    name="password"
                    label="Mot de passe"
                    required="true" />

                <x-form.button />
            </form>
            <!--end::Message-->
            <!--begin::Illustration-->
            <div class="mb-0">
                <img src="assets/media/auth/ok.png" class="mw-100 mh-300px theme-light-show" alt="" />
                <img src="assets/media/auth/ok-dark.png" class="mw-100 mh-300px theme-dark-show" alt="" />
            </div>
            <!--end::Illustration-->
        </div>
    </div>
    <!--end::Wrapper-->
@endsection

@section("script")
    <script type="text/javascript">
        let notifyCharge = document.querySelector("#notifyCharge")

        notifyCharge.innerHTML = '';

        window.setTimeout(() => {
            notifyCharge.innerHTML = "Chargement des informations..."
            window.setTimeout(() => {
                notifyCharge.innerHTML = "Vérification des informations..."

                window.setTimeout(() => {
                    notifyCharge.innerHTML = "Vérification de la connectivité avec FINTECH TELECOM..."
                    window.setTimeout(() => {
                        window.location.href='/install/param/admin'
                    }, 8000)
                }, 5800)
            }, 2500)
        }, 100)
    </script>
@endsection
