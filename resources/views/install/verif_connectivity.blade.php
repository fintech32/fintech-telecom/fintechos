@extends("install.layout")

@section("content")
    <!--begin::Wrapper-->
    <div class="card card-flush w-lg-650px py-5">
        <div class="card-body py-15 py-lg-10">
            <!--begin::Logo-->
            <div class="mb-14">
                <a href="/install" class="">
                    <img alt="Logo" src="/storage/logo_carre.png" class="h-100px" />
                </a>
            </div>
            <!--end::Logo-->
            <!--begin::Title-->
            <h1 class="fw-bolder text-gray-900 mb-5">Bienvenue</h1>
            <!--end::Title-->
            <!--begin::Message-->
            <i id="notifyIcon" class="fa fa-spinner fa-spin fs-2tx"></i>
            <div class="fs-6 fw-semibold text-gray-500 mb-10 mt-3" id="notifyCharge">
                <strong>Etape 1:</strong> Démarrage des composants
            </div>
            <!--end::Message-->
            <!--begin::Illustration-->
            <div class="mb-0">
                <img src="assets/media/auth/ok.png" class="mw-100 mh-300px theme-light-show" alt="" />
                <img src="assets/media/auth/ok-dark.png" class="mw-100 mh-300px theme-dark-show" alt="" />
            </div>
            <!--end::Illustration-->
        </div>
    </div>
    <!--end::Wrapper-->
@endsection

@section("script")
    <script type="text/javascript">
        let notifyIcon = document.querySelector("#notifyIcon")
        let notifyCharge = document.querySelector("#notifyCharge")

        //notifyCharge.innerHTML = '';

        setTimeout(() => {
            $.ajax("/api/start/component")
                .done(() => {
                    $(notifyCharge).fadeOut()
                    notifyCharge.innerHTML = '<strong>Etape 2:</strong> Recherche du signal';
                    $(notifyCharge).fadeIn()

                    $.ajax('/api/start/signal')
                        .done(() => {
                            $(notifyCharge).fadeOut()
                            notifyCharge.innerHTML = '<strong>Etape 3:</strong> Signal trouvé, établissement de la connexion';
                            $(notifyCharge).fadeIn()

                            $.ajax("/api/start/connexion")
                                .done((data) => {
                                    console.log(data)
                                    if(data != 500) {
                                        $(notifyCharge).fadeOut()
                                        notifyCharge.innerHTML = '<strong>Etape 4:</strong> Authentification de la Finbox';
                                        $(notifyCharge).fadeIn()

                                        $.ajax("/api/start/authentification")
                                            .done((data) => {
                                                $(notifyCharge).fadeOut()
                                                notifyCharge.innerHTML = '<strong>Etape 5:</strong> Connexion établie, obtention des informations serveur.';
                                                $(notifyCharge).fadeIn()


                                                $.ajax("/api/start/recuperation")
                                                    .done((data) => {
                                                        $(notifyCharge).fadeOut()
                                                        notifyCharge.innerHTML = '<strong>Etape 6:</strong> Vérification du logiciel interne et téléchargement si nécessaire.';
                                                        $(notifyCharge).fadeIn()
                                                        $.ajax('/api/start/firmware')
                                                            .done((data) => {
                                                                $(notifyCharge).fadeOut()
                                                                window.location.href="/install/param/admin"
                                                            })
                                                    })
                                            })
                                    } else {
                                        notifyIcon.classList.remove('fa-spin')
                                        notifyIcon.classList.remove('fa-spinner')
                                        notifyIcon.classList.add('fa-xmark-circle')
                                        notifyIcon.classList.add('text-danger')
                                        notifyCharge.innerHTML = "Connexion impossible"
                                    }

                                })

                        })
                        .fail(() => {
                            notifyIcon = '<i id="notifyIcon" class="fa fa-xmark-circle text-danger fs-2tx"></i>'
                        })
                })
                .fail(() => {
                    notifyIcon = '<i id="notifyIcon" class="fa fa-xmark-circle text-danger fs-2tx"></i>'
                })
        }, 100)

    </script>
@endsection
