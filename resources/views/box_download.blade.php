@extends("layouts.app")

@section("content")
    @if(!\App\Models\Config::query()->first()->transmission_install)
        <!--begin::Alert-->
        <div class="alert alert-dismissible bg-light-warning d-flex flex-center flex-column py-10 px-10 px-lg-20 mb-10">
            <!--begin::Close-->
            <button type="button" class="position-absolute top-0 end-0 m-2 btn btn-icon btn-icon-warning" data-bs-dismiss="alert">
                <i class="fa-solid fa-xmark text-warning fs-3"></i>
            </button>
            <!--end::Close-->

            <!--begin::Icon-->
            <i class="fa-solid fa-exclamation-triangle text-warning fs-5tx mb-5"></i>
            <!--end::Icon-->

            <!--begin::Wrapper-->
            <div class="text-center">


                <!--begin::Content-->
                <div class="mb-9 text-dark">
                    Le service <strong>Transmission</strong> n'est pas installé sur votre box.<br>
                    Ce service est nécessaire pour effectuer des téléchargements sur votre box.
                </div>
                <!--end::Content-->

                <!--begin::Buttons-->
                <div class="d-flex flex-center flex-wrap">
                    <button class="btn btn-warning m-2" data-bs-toggle="modal" data-bs-target="#start_install_transmission">Ok, Installer transmission</button>
                </div>
                <!--end::Buttons-->
            </div>
            <!--end::Wrapper-->
        </div>
        <!--end::Alert-->
    @else
        <div class="card card-flush shadow-sm">
            <div class="card-header">
                <h3 class="card-title">Téléchargement</h3>
                <div class="card-toolbar">

                </div>
            </div>
            <div class="card-body py-5">
                <table id="kt_datatable_zero_configuration" class="table table-row-bordered gy-5">
                    <thead>
                    <tr class="fw-semibold fs-6 text-muted">
                        <th>Nom</th>
                        <th>Taille</th>
                        <th>Status</th>
                        <th>Avancement</th>
                        <th>Start date</th>
                        <th>End date</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($transmissions as $transmission)
                    <tr>
                        <td>{{ $transmission['name'] }}</td>
                        <td>{{ \sam0hack\dconverter\DC::SizeFormat($transmission['totalSize']) }}</td>
                        <td>{{ \App\Helpers\Helpers::getStatusTransmission($transmission['status']) }}</td>
                        <td>
                            <div class="d-flex align-items-center flex-column mt-3 w-100">
                                <div class="d-flex justify-content-between fw-bold fs-6 text-success opacity-75 w-100 mt-auto mb-2">
                                    <span>64,00 Ko/s</span>
                                    <span>{{ $transmission['percentDone'] }}%</span>
                                </div>
                                <div class="h-8px mx-3 w-100 bg-success bg-opacity-50 rounded">
                                    <div class="bg-success rounded h-8px" role="progressbar" style="width: {{ $transmission['percentDone'] }}%;" aria-valuenow="{{ $transmission['percentDone'] }}" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>
                        </td>
                        <td>{{ \Carbon\Carbon::createFromTimestamp($transmission['addedDate'])->format('d/m/Y') }}</td>
                        <td>
                            @if($transmission['doneDate'] != 0)
                            {{ \Carbon\Carbon::createFromTimestamp($transmission['doneDate'])->format('d/m/Y') }}
                            @else
                            null
                            @endif
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    @endif
    <div class="modal fade" tabindex="-1" id="start_install_transmission">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title">Installation de transmission</h3>

                    <!--begin::Close-->
                    <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
                        <i class="fa-solid fa-xmark text-light-primary fs-3"></i>
                    </div>
                    <!--end::Close-->
                </div>

                <div class="modal-body">
                    <div class="d-flex align-items-center me-15 mb-3" data-bullet="install_transmission">
                        <span class="bullet bullet-dot bg-default h-15px w-15px me-5"></span> Installation de transmission
                    </div>
                    <div class="d-flex align-items-center me-15 mb-3" data-bullet="start_transmission">
                        <span class="bullet bullet-dot bg-default h-15px w-15px me-5"></span> Lancement du serveur de transmission
                    </div>
                    <div class="d-flex align-items-center me-15 mb-3" data-bullet="install_transmission_php">
                        <span class="bullet bullet-dot bg-default h-15px w-15px me-5"></span> Installation du connecteur
                    </div>
                    <div class="d-flex align-items-center me-15" data-bullet="test_transmission">
                        <span class="bullet bullet-dot bg-default h-15px w-15px me-5"></span> Test de l'installation
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-primary start_install">Lancer l'installation</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section("script")
    <script src="/assets/plugins/custom/datatables/datatables.bundle.js"></script>
    <script type="text/javascript">
        let modal = document.querySelector('#start_install_transmission')

        modal.querySelector('.start_install').addEventListener('click', e => {
            e.preventDefault()
            let bullets = {
                install_t: modal.querySelector('[data-bullet="install_transmission"]'),
                start_t: modal.querySelector('[data-bullet="start_transmission"]'),
                install_t_p: modal.querySelector('[data-bullet="install_transmission_php"]'),
                test_t: modal.querySelector('[data-bullet="test_transmission"]'),
            }

            bullets.install_t.innerHTML = `<i class="fa-solid fa-spinner fa-spin text-default me-5"></i> <strong>Installation de transmission</strong>`

            $.ajax({
                url: '/api/transmission/install_t',
                success: data => {
                    console.log(data)
                    bullets.install_t.innerHTML = `<span class="bullet bullet-dot bg-success h-15px w-15px me-5"></span> <strong class="text-success">Installation de transmission</strong>`
                    bullets.start_t.innerHTML = `<i class="fa-solid fa-spinner fa-spin text-default me-5"></i> <strong>Lancement du serveur de transmission</strong>`
                    $.ajax({
                        url: '/api/transmission/start_t',
                        success: data => {
                            bullets.start_t.innerHTML = `<span class="bullet bullet-dot bg-success h-15px w-15px me-5"></span> <strong class="text-success">Lancement du serveur de transmission</strong>`
                            bullets.install_t_p.innerHTML = `<i class="fa-solid fa-spinner fa-spin text-default me-5"></i> <strong>Installation du connecteur</strong>`
                            $.ajax({
                                url: '/api/transmission/install_php',
                                success: data => {
                                    bullets.install_t_p.innerHTML = `<span class="bullet bullet-dot bg-success h-15px w-15px me-5"></span> <strong class="text-success">Installation du connecteur</strong>`
                                    bullets.test_t.innerHTML = `<i class="fa-solid fa-spinner fa-spin text-default me-5"></i> <strong>Test de l'installation</strong>`

                                    $.ajax({
                                        url: '/api/transmission/test_transmission',
                                        success: data => {
                                            bullets.test_t.innerHTML = `<span class="bullet bullet-dot bg-success h-15px w-15px me-5"></span> <strong class="text-success">Test de l'installation</strong>`
                                            toastr.success("Système de téléchargement installée")
                                            setTimeout(() => {
                                                window.location.reload()
                                            }, 1200)
                                        },
                                        error: err => {
                                            bullets.test_t.innerHTML = `<span class="bullet bullet-dot bg-danger h-15px w-15px me-5"></span> <strong class="text-danger">Test de l'installation</strong>`
                                        }
                                    })
                                },
                                error: err => {
                                    bullets.install_t_p.innerHTML = `<span class="bullet bullet-dot bg-danger h-15px w-15px me-5"></span> <strong class="text-danger">Installation du connecteur</strong>`
                                }
                            })
                        },
                        error: err => {
                            bullets.start_t.innerHTML = `<span class="bullet bullet-dot bg-danger h-15px w-15px me-5"></span> <strong class="text-danger">Lancement du serveur de transmission</strong>`
                        }
                    })
                },
                error: err => {
                    console.error(err)
                    bullets.install_t.innerHTML = `<span class="bullet bullet-dot bg-danger h-15px w-15px me-5"></span> <strong class="text-danger">Installation de transmission</strong>`
                }
            })
        })
    </script>
@endsection
