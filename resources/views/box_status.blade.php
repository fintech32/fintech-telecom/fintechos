@extends("layouts.app")

@section("content")
    <div class="tab-content" id="myTabContent">
        <div class="tab-pane fade show active" id="diag" role="tabpanel">
            <div class="row mb-10">
                <div class="col-md-6 col-sm-12">
                    <div class="card shadow-sm">
                        <div class="card-header">
                            <h3 class="card-title">Système</h3>
                            <div class="card-toolbar">
                                <!--<button type="button" class="btn btn-sm btn-light">
                                    Action
                                </button>-->
                            </div>
                        </div>
                        <div class="card-body">
                            <table class="table table-sm border border-1 gy-5 gx-5">
                                <tbody>
                                <tr>
                                    <td>Version du firmware</td>
                                    <td>{{ $config->version_micrologiciel }}</td>
                                </tr>
                                <tr>
                                    <td>Adresse Mac</td>
                                    <td>{{ $config->mac_address }}</td>
                                </tr>
                                <tr>
                                    <td>Numéro de série</td>
                                    <td>{{ $config->serial_number }}</td>
                                </tr>
                                <tr>
                                    <td>Allumée depuis</td>
                                    <td id="uptimeNow"></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12">
                    <div class="card shadow-sm">
                        <div class="card-header">
                            <h3 class="card-title">Services</h3>
                            <div class="card-toolbar">
                                <!--<button type="button" class="btn btn-sm btn-light">
                                    Action
                                </button>-->
                            </div>
                        </div>
                        <div class="card-body">
                            <table class="table table-sm border border-1 gy-5 gx-5">
                                <tbody>
                                <tr>
                                    <td>Connexion Internet</td>
                                    <td id="upInternet"></td>
                                </tr>
                                <tr>
                                    <td>Authentification</td>
                                    <td id="upAuth"></td>
                                </tr>
                                <tr>
                                    <td>Téléphonie</td>
                                    <td id="upTel"></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card shadow-sm mb-10">
                <div class="card-header">
                    <h3 class="card-title">Température</h3>
                    <div class="card-toolbar">
                        <!--<button type="button" class="btn btn-sm btn-light">
                            Action
                        </button>-->
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6 col-sm-12">
                            <div class="d-flex flex-column">
                                <div class="d-flex flex-row justify-content-between mb-5">
                                    <div class="fw-bolder">Disque Dur</div>
                                    <div class="fs-7" id="tempHdd"></div>
                                </div>
                                <div class="d-flex flex-row justify-content-between mb-5">
                                    <div class="fw-bolder">Switch</div>
                                    <div class="fs-7" id="tempSwitch"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <div class="d-flex flex-column">
                                <div class="d-flex flex-row justify-content-between mb-5">
                                    <div class="fw-bolder">CPU A</div>
                                    <div class="fs-7" id="tempCpua"></div>
                                </div>
                                <div class="d-flex flex-row justify-content-between mb-5">
                                    <div class="fw-bolder">CPU B</div>
                                    <div class="fs-7" id="tempCpub"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card shadow-sm">
                <div class="card-header">
                    <h3 class="card-title">Ventilateur</h3>
                    <div class="card-toolbar">
                        <!--<button type="button" class="btn btn-sm btn-light">
                            Action
                        </button>-->
                    </div>
                </div>
                <div class="card-body">
                    <table class="table table-sm gy-5 gx-5">
                        <tbody>
                        <tr>
                            <td class="fw-bolder">Ventilateur 1</td>
                            <td id="sensorVone"></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="tab-pane fade" id="status" role="tabpanel">
            <div class="row">
                <div class="col-md-6 col-sm-12">
                    <div class="card shadow-sm">
                        <div class="card-header">
                            <h3 class="card-title">Etat de la connexion internet</h3>
                            <div class="card-toolbar">
                                <!--<button type="button" class="btn btn-sm btn-light">
                                    Action
                                </button>-->
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="d-flex flex-row justify-content-between align-items-center mb-5">
                                <strong>Etat de la connexion:</strong> <span id="etat_connexion_internet">{!! $statement->etat_connexion_internet_format !!}</span>
                            </div>
                            <div class="d-flex flex-row justify-content-between align-items-center mb-5">
                                <strong>Type de connexion:</strong> <span>{{ Str::upper($config->type_connect) }}</span>
                            </div>
                            <div class="d-flex flex-row justify-content-between align-items-center mb-5">
                                <strong>Adresse IPV4:</strong> <span>{{ $statement->ipv4 }}</span>
                            </div>
                            <div class="d-flex flex-row justify-content-between align-items-center mb-5">
                                <strong>Adresse IPV6:</strong> <span>{{ $statement->ipv6 }}</span>
                            </div>
                            <div class="d-flex flex-row justify-content-between align-items-center mb-5">
                                <strong>Reçu:</strong> <span>{{ \App\VerifInfoTelecom::convertByte($statement->recu, "Mo", "Go") }} Go</span>
                            </div>
                            <div class="d-flex flex-row justify-content-between align-items-center mb-5">
                                <strong>Emis:</strong> <span>{{ \App\VerifInfoTelecom::convertByte($statement->emis, "Mo", "Go") }} Go</span>
                            </div>
                            <div class="d-flex flex-row justify-content-between align-items-center mb-5">
                                <strong>Débit Ethernet descendant:</strong> <span>{{ \App\VerifInfoTelecom::convertByte($statement->debitDown, "Mo", "Go") }} Go</span>
                            </div>
                            <div class="d-flex flex-row justify-content-between align-items-center mb-5">
                                <strong>Débit Ethternet montant:</strong> <span>{{ \App\VerifInfoTelecom::convertByte($statement->debitUp, "Mo", "Go") }} Go</span>
                            </div>
                            <div class="d-flex flex-row justify-content-between align-items-center mb-5">
                                <strong>Débit Ethernet descendant Max:</strong> <span>{{ \App\VerifInfoTelecom::convertByte($statement->debitDownMax, "Mo", "Go") }} Go</span>
                            </div>
                            <div class="d-flex flex-row justify-content-between align-items-center mb-5">
                                <strong>Débit Ethternet montant Max:</strong> <span>{{ \App\VerifInfoTelecom::convertByte($statement->debitUpMax, "Mo", "Go") }} Mo</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12">
                    <div class="card shadow-sm">
                        <div class="card-header">
                            <h3 class="card-title">Etat de la connexion {{ $config->type_connect }}</h3>
                            <div class="card-toolbar">
                                <!--<button type="button" class="btn btn-sm btn-light">
                                    Action
                                </button>-->
                            </div>
                        </div>
                        <div class="card-body">
                            @if($config->type_connect == 'ftth')
                            <div class="d-flex flex-row justify-content-between align-items-center mb-5">
                                <strong>Etat du lien:</strong> {!! $statement->etat_connexion_ftth_format !!}
                            </div>
                            <div class="d-flex flex-row justify-content-between align-items-center mb-5">
                                <strong>Module Connecter:</strong> {!! $statement->module_connect_format !!}
                            </div>
                            @endif

                            @if($config->type_connect == 'adsl')
                                <div class="d-flex flex-row justify-content-between align-items-center mb-5">
                                    <strong>Etat du lien:</strong> {!! $statement->etat_connexion_adsl_format !!}
                                </div>
                                <div class="d-flex flex-row justify-content-between align-items-center mb-5">
                                    <strong>Protocol:</strong> ADSL2
                                </div>
                                <div class="d-flex flex-row justify-content-between align-items-center mb-5">
                                    <strong>Synchro depuis:</strong> {!! $statement->start_adsl_synchro !!}
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-pane fade" id="logs" role="tabpanel">
            <div class="card shadow-sm">
                <div class="card-header">
                    <h3 class="card-title">Historique de connexion</h3>
                    <div class="card-toolbar">
                        <!--<button type="button" class="btn btn-sm btn-light">
                            Action
                        </button>-->
                    </div>
                </div>
                <div class="card-body">
                    <table class="table gy-5 gx-5">
                        <thead>
                            <tr>
                                <th>Date</th>
                                <th>Type</th>
                                <th>Nom</th>
                                <th>Etat</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach(\App\Models\LogConnect::all() as $log)
                                <tr>
                                    <td>{{ $log->created_at->format("d/m/Y à H:i") }}</td>
                                    <td>{{ $log->type }}</td>
                                    <td>{{ $log->lien }}</td>
                                    <td>{{ $log->status }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section("script")
    <script type="text/javascript">
        let uptimeDiv = document.querySelector("#uptimeNow")
        let upInternet = document.querySelector("#upInternet")
        let upAuth = document.querySelector("#upAuth")
        let upTel = document.querySelector("#upTel")
        let tempHdd = document.querySelector("#tempHdd")
        let tempSwitch = document.querySelector("#tempSwitch")
        let tempCpua = document.querySelector("#tempCpua")
        let tempCpub = document.querySelector("#tempCpub")
        let sensorVone = document.querySelector("#sensorVone")

        let updateNow = () => {
            moment.locale('fr')
            uptimeDiv.innerHTML = moment('{{ $statement->start_at->format("Y-m-d H:i:s") }}').fromNow()
        }

        let updateTemperatureHdd = () => {
            $.ajax({
                url: '/api/sensor',
                method: 'POST',
                data: {"sensor": "tempHdd"},
                success: data => {
                    tempHdd.innerHTML = data
                }
            })
        }

        let updateTemperatureSwitch = () => {
            $.ajax({
                url: '/api/sensor',
                method: 'POST',
                data: {"sensor": "tempSwitch"},
                success: data => {
                    tempSwitch.innerHTML = data
                }
            })
        }

        let updateTemperatureCpua = () => {
            $.ajax({
                url: '/api/sensor',
                method: 'POST',
                data: {"sensor": "tempCpua"},
                success: data => {
                    tempCpua.innerHTML = data
                }
            })
        }

        let updateTemperatureCpub = () => {
            $.ajax({
                url: '/api/sensor',
                method: 'POST',
                data: {"sensor": "tempCpub"},
                success: data => {
                    tempCpub.innerHTML = data
                }
            })
        }
        let updatesensorVone= () => {
            $.ajax({
                url: '/api/sensor',
                method: 'POST',
                data: {"sensor": "sensorVone"},
                success: data => {
                    sensorVone.innerHTML = data
                }
            })
        }
        updateNow()
        updateTemperatureHdd()
        updateTemperatureSwitch()
        updateTemperatureCpua()
        updateTemperatureCpub()
        updatesensorVone()
        setInterval(() => {
            updateNow()
            upInternet.innerHTML = "{!! $statement->stat_internet_format  !!}"
            upAuth.innerHTML = "{!! $statement->stat_auth_format  !!}"
            upTel.innerHTML = "{!! $statement->stat_telephone_format  !!}"
        }, 1000)
    </script>
@endsection
