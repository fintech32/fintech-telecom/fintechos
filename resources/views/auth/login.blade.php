@extends("install.layout")

@section("content")
    <!--begin::Wrapper-->
    <div class="card card-flush w-lg-650px py-5">
        <div class="card-body py-15 py-lg-10">
            <!--begin::Logo-->
            <div class="mb-14">
                <a href="/install" class="">
                    <img alt="Logo" src="/storage/logo_carre.png" class="h-100px" />
                </a>
            </div>
            <!--end::Logo-->
            <!--begin::Title-->
            <h1 class="fw-bolder text-gray-900 mb-5">Connexion</h1>
            <!--end::Title-->
            <!--begin::Message-->

            <form action="{{ route('login') }}" class="my-5" method="POST">
                @csrf
                <x-form.input
                    name="name"
                    label="Pseudo"
                    required="true" />

                <x-form.input
                    type="password"
                    name="password"
                    label="Mot de passe"
                    required="true" />


                <x-form.button />
            </form>
            <!--end::Message-->
            <!--begin::Illustration-->
            <div class="mb-0">
                <img src="assets/media/auth/ok.png" class="mw-100 mh-300px theme-light-show" alt="" />
                <img src="assets/media/auth/ok-dark.png" class="mw-100 mh-300px theme-dark-show" alt="" />
            </div>
            <!--end::Illustration-->
        </div>
    </div>
    <!--end::Wrapper-->
@endsection

@section("script")

@endsection
