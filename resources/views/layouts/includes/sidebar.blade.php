<!--begin::Sidebar-->
<div id="kt_app_sidebar" class="app-sidebar flex-column" data-kt-drawer="true" data-kt-drawer-name="app-sidebar" data-kt-drawer-activate="{default: true, lg: false}" data-kt-drawer-overlay="true" data-kt-drawer-width="275px" data-kt-drawer-direction="start" data-kt-drawer-toggle="#kt_app_sidebar_toggle">
    <!--begin::Logo-->
    <div class="d-flex flex-stack px-4 px-lg-6 py-3 py-lg-8" id="kt_app_sidebar_logo">
        <!--begin::Logo image-->
        <a href="/">
            <img alt="Logo" src="/storage/logo_long.png" class="h-20px h-lg-70px theme-light-show" />
            <img alt="Logo" src="/storage/logo_long.png" class="h-20px h-lg-70px theme-dark-show" />
        </a>
        <!--end::Logo image-->

    </div>
    <!--end::Logo-->
    <!--begin::Sidebar nav-->
    <div class="flex-column-fluid px-4 px-lg-8 py-4" id="kt_app_sidebar_nav">
        <!--begin::Nav wrapper-->
        <div id="kt_app_sidebar_nav_wrapper" class="d-flex flex-column hover-scroll-y pe-4 me-n4" data-kt-scroll="true" data-kt-scroll-activate="true" data-kt-scroll-height="auto" data-kt-scroll-dependencies="#kt_app_sidebar_logo, #kt_app_sidebar_footer" data-kt-scroll-wrappers="#kt_app_sidebar, #kt_app_sidebar_nav" data-kt-scroll-offset="5px">

            <!--begin::Links-->
            <div class="mb-6">
                <!--begin::Row-->
                <div class="row row-cols-2 row-cols-lg-3" data-kt-buttons="true" data-kt-buttons-target="[data-kt-button]">
                    <!--begin::Col-->
                    <div class="col mb-4">
                        <!--begin::Link-->
                        <a href="{{ route('box_status') }}" class="btn btn-icon btn-outline {{ route_is('box_status') ? 'btn-bg-primary text-white' : 'btn-bg-light' }} btn-active-light-primary btn-flex flex-column flex-center w-90px h-90px border-gray-200 " data-kt-button="true">
                            <!--begin::Icon-->
                            <span class="mb-2">
								<i class="fa fa-heart-pulse {{ route_is('box_status') ? 'text-white' : '' }} fs-2"></i>
							</span>
                            <!--end::Icon-->
                            <!--begin::Label-->
                            <span class="fs-7 fw-bold">Etat de la finbox</span>
                            <!--end::Label-->
                        </a>
                        <!--end::Link-->
                    </div>
                    <div class="col mb-4">
                        <!--begin::Link-->
                        <a href="{{ route('box_download') }}" class="btn btn-icon btn-outline {{ route_is('box_download') ? 'btn-bg-primary text-white' : 'btn-bg-light' }} btn-active-light-primary btn-flex flex-column flex-center w-auto h-90px border-gray-200" data-kt-button="true">
                            <!--begin::Icon-->
                            <span class="mb-2">
								<i class="fa fa-bars-progress {{ route_is('box_download') ? 'text-white' : '' }} fs-2"></i>
							</span>
                            <!--end::Icon-->
                            <!--begin::Label-->
                            <span class="fs-7 fw-bold">Téléchargement</span>
                            <!--end::Label-->
                        </a>
                        <!--end::Link-->
                    </div>
                    <div class="col mb-4">
                        <!--begin::Link-->
                        <button href="../../demo23/dist/apps/calendar.html" class="btn btn-icon btn-outline btn-bg-light btn-active-light-primary btn-flex flex-column flex-center w-auto h-90px border-gray-200" data-kt-button="true" disabled data-bs-toggle="tooltip" title="prochainement">
                            <!--begin::Icon-->
                            <span class="mb-2">
								<i class="fa fa-folder fs-2"></i>
							</span>
                            <!--end::Icon-->
                            <!--begin::Label-->
                            <span class="fs-7 fw-bold">Explorateur de fichier</span>
                            <!--end::Label-->
                        </button>
                        <!--end::Link-->
                    </div>
                    <div class="col mb-4">
                        <!--begin::Link-->
                        <button href="../../demo23/dist/apps/calendar.html" class="btn btn-icon btn-outline btn-bg-light btn-active-light-primary btn-flex flex-column flex-center w-auto h-90px border-gray-200" data-kt-button="true" disabled data-bs-toggle="tooltip" title="prochainement">
                            <!--begin::Icon-->
                            <span class="mb-2">
								<i class="fa fa-cogs fs-2"></i>
							</span>
                            <!--end::Icon-->
                            <!--begin::Label-->
                            <span class="fs-7 fw-bold">Paramètre de la finbox</span>
                            <!--end::Label-->
                        </button>
                        <!--end::Link-->
                    </div>
                    <div class="col mb-4">
                        <!--begin::Link-->
                        <button href="../../demo23/dist/apps/calendar.html" class="btn btn-icon btn-outline btn-bg-light btn-active-light-primary btn-flex flex-column flex-center w-auto h-90px border-gray-200" data-kt-button="true" disabled data-bs-toggle="tooltip" title="prochainement">
                            <!--begin::Icon-->
                            <span class="mb-2">
								<i class="fa fa-network-wired fs-2"></i>
							</span>
                            <!--end::Icon-->
                            <!--begin::Label-->
                            <span class="fs-7 fw-bold">Périphérique réseau</span>
                            <!--end::Label-->
                        </button>
                        <!--end::Link-->
                    </div>
                    <div class="col mb-4">
                        <!--begin::Link-->
                        <button href="../../demo23/dist/apps/calendar.html" class="btn btn-icon btn-outline btn-bg-light btn-active-light-primary btn-flex flex-column flex-center w-auto h-90px border-gray-200" data-kt-button="true" disabled data-bs-toggle="tooltip" title="prochainement">
                            <!--begin::Icon-->
                            <span class="mb-2">
								<i class="fa fa-phone-volume fs-2"></i>
							</span>
                            <!--end::Icon-->
                            <!--begin::Label-->
                            <span class="fs-7 fw-bold">Journal d'appel</span>
                            <!--end::Label-->
                        </button>
                        <!--end::Link-->
                    </div>
                    <!--end::Col-->
                </div>
                <!--end::Row-->
            </div>
            <!--end::Links-->
            @if(route_is('box_status'))
                <ul class="nav nav-tabs nav-pills border-0 flex-row flex-md-column me-5 mb-3 mb-md-0 fs-6">
                    <li class="nav-item w-md-200px me-0">
                        <a class="nav-link active" data-bs-toggle="tab" href="#diag">Diagnostique</a>
                    </li>
                    <li class="nav-item w-md-200px me-0">
                        <a class="nav-link" data-bs-toggle="tab" href="#status">Etat Internet</a>
                    </li>
                    <li class="nav-item w-md-200px">
                        <a class="nav-link" data-bs-toggle="tab" href="#logs">Historique</a>
                    </li>
                </ul>
            @endif
        </div>
        <!--end::Nav wrapper-->
    </div>
    <!--end::Sidebar nav-->
</div>
<!--end::Sidebar-->
