<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('/start/component', function () {
    sleep(2);
    return response()->json();
});
Route::get('/start/signal', function () {
    sleep(3);
    return response()->json();
});
Route::get('/start/connexion', function () {
    $telecom = new \App\Service\Telecom();
    return $telecom->connexion_serveur(\App\Models\Config::query()->first()->customer_code);
});
Route::get('/start/authentification', function () {
    if(config('app.env') == 'local') {
        return response()->json();
    } else {
        $telecom = new \App\Service\Telecom();
        $xdsl = $telecom->authentification(\App\Models\Config::query()->first()->customer_code)->data->xdsl;
        if($xdsl->status == 'active') {
            return response()->json();
        } else {
            return response()->json(null, 500);
        }
    }
});
Route::get('/start/recuperation', function () {
    if(config('app.env') == 'local') {
        $faker = \Faker\Factory::create('fr_FR');
        \App\Models\Config::query()->first()->update([
            'mac_address' => $faker->macAddress,
            'serial_number' => $faker->randomNumber(3).'-'.$faker->randomNumber(7).'-'.$faker->randomNumber(5),
            'type_connect' => 'adsl'
        ]);
        return response()->json();
    } else {
        $telecom = new \App\Service\Telecom();
        $xdsl = $telecom->authentification(\App\Models\Config::query()->first()->customer_code)->data->xdsl;
        if($xdsl->status == 'active') {
            \App\Models\Config::query()->first()->update([
                "mac_address" => $xdsl->modems[0]->mac_address,
                "serial_number" => $xdsl->modems[0]->serial_number,
                "type_connect" => $xdsl->type_access
            ]);
            return response()->json();
        } else {
            return response()->json(null, 500);
        }
    }
});
Route::get('/start/firmware', function () {
    if(config('app.env') == 'local') {
        return response()->json();
    } else {
        $telecom = new \App\Service\Telecom();
        $xdsl = $telecom->authentification(\App\Models\Config::query()->first()->customer_code)->data->xdsl;
        $config = \App\Models\Config::query()->first();

        if($config->version_micrologiciel < $xdsl->modems[0]->firmwareVersion) {
            $process = \Symfony\Component\Process\Process::fromShellCommandline('/bin/bash ./update.sh');
            if(!$process->getExitCode()) {
                $config->update(["version_micrologiciel" => $xdsl->modems[0]->firmwareVersion]);
                return response()->json();
            } else {
                return response()->json(null, 500);
            }
        } else {
            return response()->json();
        }
    }
});
Route::post('/sensor', [\App\Http\Controllers\SensorController::class, 'sensor'])->name('sensor');

Route::prefix('transmission')->group(function () {
    Route::get('install_t', [\App\Http\Controllers\Install\TransmissionController::class, 'install']);
    Route::get('start_t', [\App\Http\Controllers\Install\TransmissionController::class, 'start_t']);
    Route::get('install_php', [\App\Http\Controllers\Install\TransmissionController::class, 'install_php']);
    Route::get('test_transmission', [\App\Http\Controllers\Install\TransmissionController::class, 'test_transmission']);
    Route::post('/', [\App\Http\Controllers\Install\TransmissionController::class, 'add_torrent']);
});

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
