<?php

declare(strict_types=1);

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('install')->name('install.')->group(function (): void {
    Route::get('/', [\App\Http\Controllers\Install\InstallController::class, 'verifyConnectivity'])->name('verify-connectivity');
    Route::post('/', [\App\Http\Controllers\Install\InstallController::class, 'store'])->name('store');
    Route::get('/param/admin', [\App\Http\Controllers\Install\InstallController::class, 'administrateur'])->name('administrateur');
    Route::get('/param/wifi', [\App\Http\Controllers\Install\InstallController::class, 'wifi'])->name('wifi');
});
Route::get('/test', function () {
    dd(database_path());
});

Route::middleware(["installed"])->group(function (): void {
    Route::get("/", [\App\Http\Controllers\BaseController::class, 'index']);
    Route::get('/box/status', [\App\Http\Controllers\BaseController::class, 'box_status'])->name('box_status');
    Route::get('/box/download', [\App\Http\Controllers\BaseController::class, 'box_download'])->name('box_download');
});
