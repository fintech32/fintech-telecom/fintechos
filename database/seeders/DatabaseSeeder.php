<?php

declare(strict_types=1);

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Models\Config;
use App\Models\Statement;
use App\Models\Wifi;
use Faker\Factory;
use Illuminate\Database\Seeder;
use Str;

final class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run(): void
    {
        $faker = Factory::create('fr_FR');
        $type_con = ["adsl", "ftth"];
        $t = $type_con[rand(0, 1)];
        Config::create([
            "installer" => false,
            "type_connect" => $t,
            "customer_code" => "CUS_MM789547",
        ]);
        $ssid = "Finbox-".Str::upper(Str::random(6));
        $key = encrypt(Str::random(35));
        Wifi::create([
            "name_connection" => "Carte Wi-fi 2.4Ghz / 802.11g",
            "bande" => "2.4Ghz",
            "canal_primary" => 6,
            "active" => false,
            "ssid" => $ssid,
            "key" => $key,
            "type_protection" => "wpa2aes",
            "wps" => true
        ]);
        Wifi::create([
            "name_connection" => "Carte Wi-fi 5Ghz / 802.11ac",
            "bande" => "5Ghz",
            "canal_primary" => 36,
            "active" => false,
            "ssid" => $ssid,
            "key" => $key,
            "type_protection" => "wpa2aes",
            "wps" => true
        ]);
        Statement::query()->create([
            "start_at" => now(),
            "stat_internet" => "ok",
            "stat_auth" => "ok",
            "stat_telephone" => "ok",
            "etat_connexion_internet" => "connecter",
            "etat_connexion_ftth" => 'ftth' === $t ? "connecter" : "deconnecter",
            "etat_connexion_adsl" => 'adsl' === $t ? "connecter" : "deconnecter",
            "type_connexion" => $t,
            "ipv4" => $faker->ipv4,
            "ipv6" => $faker->ipv6,
            "recu" => $faker->randomFloat(2, 0, 3),
            "emis" => $faker->randomFloat(2, 0, 3),
            "debitDown" => $faker->randomFloat(2, 0, 5),
            "debitUp" => $faker->randomFloat(2, 0, 100),
            "debitUpMax" => 700,
            "debitDownMax" => 1,
            "module_connect" => 'ftth' === $t,
        ]);
    }
}
