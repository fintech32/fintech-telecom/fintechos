<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

final class Statement extends Model
{
    protected $guarded = [];
    public $timestamps = false;
    protected $dates = ["start_at"];
}
