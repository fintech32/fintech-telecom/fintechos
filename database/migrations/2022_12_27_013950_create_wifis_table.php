<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    public function up(): void
    {
        Schema::create('wifis', function (Blueprint $table): void {
            $table->id();
            $table->string('name_connection');
            $table->string('bande');
            $table->string('canal_primary');
            $table->boolean('active')->default(true);
            $table->string('ssid');
            $table->enum('type_protection', [
                "wpa2aes",
                "wpa3aes"
            ]);
            $table->text('key');
            $table->boolean('wps')->default(true);
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('wifis');
    }
};
