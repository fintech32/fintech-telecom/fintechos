<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    public function up(): void
    {
        Schema::create('configs', function (Blueprint $table): void {
            $table->id();
            $table->boolean('installer')->default(false);
            $table->string('version_micrologiciel')->default('1.0.0');
            $table->macAddress()->nullable();
            $table->string('serial_number')->nullable();
            $table->enum('type_connect', ["adsl", "ftth"]);

            $table->string('customer_code')->nullable();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('configs');
    }
};
