<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    public function up(): void
    {
        Schema::create('statements', function (Blueprint $table): void {
            $table->id();
            $table->timestamp('start_at');
            $table->enum('stat_internet', [
                "ok",
                "echec"
            ]);
            $table->enum('stat_auth', [
                "ok",
                "echec"
            ]);
            $table->enum('stat_telephone', [
                "ok",
                "echec"
            ]);
            $table->enum("etat_connexion_internet", ["connecter", "deconnecter"]);
            $table->enum("etat_connexion_ftth", ["connecter", "deconnecter"])->nullable();
            $table->enum("etat_connexion_adsl", ["connecter", "deconnecter"])->nullable();
            $table->timestamp('start_adsl_synchro')->nullable();

            $table->enum('type_connexion', ["adsl", "ftth", "vdsl"]);
            $table->ipAddress('ipv4');
            $table->string('ipv6')->nullable();
            $table->float('recu');
            $table->float('emis');
            $table->float('debitDown');
            $table->float('debitUp');
            $table->float('debitDownMax');
            $table->float('debitUpMax');

            $table->boolean('module_connect')->default(true);
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('statements');
    }
};
